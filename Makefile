run:
	@./gradlew bootRun

test:
	@./gradlew clean test

.PHONY: run test