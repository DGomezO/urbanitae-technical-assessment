package com.urbanitae.assessment.domain.common.util;

/**
 * Utility methods to work with {@link String}
 *
 * @author David G. Ortiz
 */
public final class StringUtils {

    private StringUtils() {
    }

    /**
     * <p>Determines if a given {@link String} is a palindrome
     * (a word or sentence that reads the same backward or forward)</p>
     *
     * <p>Returns false if null or empty input</p>
     *
     * @param str the string to check
     * @return {@code true} if the string is a palindrome
     */
    public static boolean isPalindrome(String str) {

        if (isBlank(str)) {
            return false;
        }

        String withoutSpaces = str.replace(" ", "");
        return equalsIgnoreCase(withoutSpaces, reverse(withoutSpaces));
    }

    /**
     * Returns a reversed representation of a given string
     * using a given character as a delimiter
     *
     * @param str       the string to reverse
     * @param delimiter the delimiter character to use
     * @return a reversed representation of the string
     */
    public static String reverseBy(String str, char delimiter) {

        if (isBlank(str)) {
            return str;
        }

        StringBuilder reversed = new StringBuilder();

        String[] splitted = str.split(String.valueOf(delimiter));
        for (int i = 0; i < splitted.length; i++) {
            String word = i > 0 ? splitted[i] + delimiter : splitted[i];
            reversed.insert(0, word);
        }

        return reversed.toString();
    }

    /**
     * Returns a reversed representation of a given string
     *
     * @param str the string to reverse
     * @return a reversed representation of the string
     */
    public static String reverse(String str) {

        if (isBlank(str)) {
            return str;
        }

        return new StringBuilder(str).reverse().toString();
    }

    /**
     * Determines if a given {@link String} is either {@code null}
     * or empty (contains only whitespace characters).
     *
     * @param str the string to check
     * @return {@code true} if the string is either null or empty
     */
    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    /**
     * Determines if two given {@link String} are equal
     * (case insensitive)
     *
     * @param str1 the first string to compare
     * @param str2 the second string to compare
     * @return {@code true} if the strings are equal or both {@code null}
     */
    public static boolean equalsIgnoreCase(String str1, String str2) {

        if (str1 == null && str2 == null) {
            return true;
        } else if (str1 == null || str2 == null) {
            return false;
        }

        return str1.equalsIgnoreCase(str2);
    }
}
