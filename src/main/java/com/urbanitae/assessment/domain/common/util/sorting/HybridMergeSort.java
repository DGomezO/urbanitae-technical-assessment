package com.urbanitae.assessment.domain.common.util.sorting;

import com.urbanitae.assessment.domain.common.util.IntArrayUtils;
import org.springframework.util.Assert;

/**
 * <p>Sorts a given int array by using a hybrid
 * algorith to improve performance</p>
 *
 * <p>{@link HybridQuickSort} is used for small and medium size arrays or
 * array partitions because it offers a linear O(n) or logarithmic Θ(n log(n))
 * performance for its best cases, but for longer datasets it degrades to 0(n2)
 * performance due to the high amount of comparisons that it needs to do</p>
 *
 * <p>An implementation of Merge Sort is used those for largest arrays because
 * it offers a consistent logarithmic performance Θ(n log(n)) for any array size</p>
 *
 * @author David G. Ortiz
 */
public final class HybridMergeSort {

    private static final int QUICKSORT_THRESHOLD = 286;

    private HybridMergeSort() {
    }

    /**
     * Sorts a given int array by using
     * the MergeSort algorithm
     *
     * @param arr the int array to sort
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr) {

        Assert.notNull(arr, "An array is required");

        sort(arr, 0, arr.length - 1);
    }

    private static void sort(int[] arr, int startIdx, int endIdx) {

        if (startIdx < endIdx) { // Condition to stop recursion if the array is already sorted

            if ((endIdx - startIdx) < QUICKSORT_THRESHOLD) {
                /* Quick sort average time complexity is Θ(n log(n)), but for long arrays it
                degrades to 0(n2) because of the high amount of comparisons that it needs to do.
                That's why we use it only for medium size datasets, where it is faster and more
                efficient than merge sort */
                HybridQuickSort.sort(arr, startIdx, endIdx);
            } else {
                int midIdx = (startIdx + endIdx) / 2; // Calculate the middle index of the array

                sort(arr, startIdx, midIdx); // Recursively sort the left half
                sort(arr, midIdx + 1, endIdx); // Recursively sort the right half

                merge(arr, startIdx, midIdx, endIdx); // Merge both halves into the original array
            }
        }
    }

    private static void merge(int[] arr, int startIdx, int midIdx, int endIdx) {

        // Create new arrays for left and right halves
        int[] leftArray = IntArrayUtils.copyOf(arr, startIdx, midIdx + 1);
        int[] rightArray = IntArrayUtils.copyOf(arr, midIdx + 1, endIdx + 1);

        // Calculate the size of both halves
        int leftArrLength = leftArray.length;
        int rightArrLength = rightArray.length;

        /* Initialize the index that will be used for sorting
        the original array and two indexes to iterate over the
        left and right halves arrays */
        int sortingIdx = startIdx;
        int leftArrayIdx = 0;
        int rightArrayIdx = 0;

        // Iterate over both halves arrays until reaching the end of any of them
        while (leftArrayIdx < leftArrLength && rightArrayIdx < rightArrLength) {

            /* Compare the values in the current loop indexes of both arrays.
            If the value from left array is smaller or equal than the value from
            the right array, place that value in the current sorting index of the
            original array and increment the left array index. Else, place the value
            from the right array in the current sorting index of the original array
            and increment the right array index. */
            if (leftArray[leftArrayIdx] <= rightArray[rightArrayIdx]) {
                arr[sortingIdx] = leftArray[leftArrayIdx];
                leftArrayIdx++;
            } else {
                arr[sortingIdx] = rightArray[rightArrayIdx];
                rightArrayIdx++;
            }

            /* Finally, no matter which half's value has been placed
            in the original array, increment the current sorting index */
            sortingIdx++;
        }

        // If there are remaining values in the left
        // array, place them in the original array
        while (leftArrayIdx < leftArrLength) {
            arr[sortingIdx] = leftArray[leftArrayIdx];
            leftArrayIdx++;
            sortingIdx++;
        }

        // If there are remaining values in the right
        // array, place them in the original array
        while (rightArrayIdx < rightArrLength) {
            arr[sortingIdx] = rightArray[rightArrayIdx];
            rightArrayIdx++;
            sortingIdx++;
        }
    }
}
