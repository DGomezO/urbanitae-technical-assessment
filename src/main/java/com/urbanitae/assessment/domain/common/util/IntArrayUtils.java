package com.urbanitae.assessment.domain.common.util;

import com.urbanitae.assessment.domain.common.util.sorting.HybridMergeSort;
import org.springframework.util.Assert;

import java.util.Random;

/**
 * Utility methods to work with arrays of integers
 *
 * @author David G. Ortiz
 */
public final class IntArrayUtils {

    private IntArrayUtils() {
    }

    /**
     * <p>Sorts a given int array by using a hybrid
     * algorith, {@link HybridMergeSort}, to improve performance</p>
     *
     * <p>Insertion Sort is used for the smallest arrays or array partitions
     * because it offers a linear O(n) performance for its best cases, but
     * degrades significantly for bigger datasets</p>
     *
     * <p>Quick Sort is used for medium size arrays or array partitions
     * because it offers a logarithmic Θ(n log(n)) performance for its
     * best cases, but for longer datasets it degrades to 0(n2) performance
     * due to the high amount of comparisons that it needs to do</p>
     *
     * <p>Merge Sort is used for largest arrays because it offers a consistent
     * logarithmic performance Θ(n log(n)) for any array size</p>
     *
     * @param arr the int array to sort
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr) {
        HybridMergeSort.sort(arr);
    }

    /**
     * Shuffles a given int array, randomly swapping its elements positions
     *
     * @param arr the int array to shuffle
     * @throws IllegalArgumentException if arr is null
     */
    public static void shuffle(int[] arr) {

        Assert.notNull(arr, "An array is required");

        Random rand = new Random();

        for (int i = 0; i < arr.length; i++) {
            swap(arr, i, rand.nextInt(arr.length));
        }
    }

    /**
     * Swap the positions of two elements of an int array
     *
     * @param arr       the int array which elements should be swapped
     * @param firstIdx  the index of the first element
     * @param secondIdx the index of the second element
     * @throws IllegalArgumentException if arr is null
     * @throws ArrayIndexOutOfBoundsException if any of the indexes are out of bounds
     */
    public static void swap(int[] arr, int firstIdx, int secondIdx) {

        Assert.notNull(arr, "An array is required");

        int firstIdxInitialValue = arr[firstIdx];

        arr[firstIdx] = arr[secondIdx];
        arr[secondIdx] = firstIdxInitialValue;
    }

    /**
     * Creates a copy of a given int array
     *
     * @param original the int array to copy
     * @return a copy of the original int array
     * @throws IllegalArgumentException if the original array is null
     */
    public static int[] copyOf(int[] original) {

        Assert.notNull(original, "An array is required");

        return copyOf(original, 0, original.length);
    }

    /**
     * Creates a copy of a given int array
     * from a starting to an ending position
     *
     * @param original the int array to copy from
     * @param startIdx the starting index (included)
     * @param endIdx the ending index (excluded)
     * @return a copy of the original int array from the
     * starting (included) to the ending (excluded) index
     * @throws IllegalArgumentException if the original array is null or
     * startIdx is greater than endIdx
     * @throws ArrayIndexOutOfBoundsException if startIdx is less than 0 or
     * endIdx is greater than the original array length
     */
    public static int[] copyOf(int[] original, int startIdx, int endIdx) {

        Assert.notNull(original, "An array is required");

        int newLength = endIdx - startIdx;
        if (newLength < 0) {
            throw new IllegalArgumentException("The startIdx cannot be greater than the endIdx");
        }

        int[] copy = new int[newLength];
        System.arraycopy(original, startIdx, copy, 0, newLength);
        return copy;
    }
}
