package com.urbanitae.assessment.domain.common.util.sorting;

import com.urbanitae.assessment.domain.common.util.IntArrayUtils;
import org.springframework.util.Assert;

/**
 * <p>Sorts a given int array by using a hybrid
 * algorith to improve performance</p>
 *
 * <p>{@link InsertionSort} is used for the smallest arrays or array partitions
 * because it offers a linear O(n) performance for its best cases, but
 * degrades significantly for bigger datasets</p>
 *
 * <p>An implementation of Quick Sort algorithm is used for larger arrays or
 * array partitions because it offers a logarithmic Θ(n log(n)) performance</p>
 *
 * <p>Note that Quick Sort degrades to 0(n2) performance
 * due to the high amount of comparisons that it needs to do, so you should
 * consider using {@link HybridMergeSort} for really large datasets</p>
 *
 * @author David G. Ortiz
 */
public final class HybridQuickSort {

    private static final int INSERTION_SORT_THRESHOLD = 47;

    private HybridQuickSort() {
    }

    /**
     * Sorts a given int array by using
     * the QuickSort algorithm
     *
     * @param arr the int array to sort
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr) {

        Assert.notNull(arr, "An array is required");

        sort(arr, 0, arr.length - 1);
    }

    /**
     * Sorts a given int array from a
     * startIdx to an endIdx by using
     * the QuickSort algorithm
     *
     * @param arr the int array to sort
     * @param startIdx the starting index
     * @param endIdx the ending index
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr, int startIdx, int endIdx) {

        if (startIdx < endIdx) {  // Condition to stop recursion if the array is already sorted

            if ((endIdx - startIdx) < INSERTION_SORT_THRESHOLD) {
                /* Use insertion sort for small array partitions
                because its time complexity is linear O(n) for those best cases */
                InsertionSort.sort(arr, startIdx, endIdx);
            } else {

                /* Choose a pivot value and divide the array in two partitions,
                one with those values which are smaller than the pivot and the
                other with those that are bigger */
                int partitionIdx = partition(arr, startIdx, endIdx);

                // Recursively sort the left partition
                sort(arr, startIdx, partitionIdx - 1);

                // Recursively sort the right partition
                sort(arr, partitionIdx + 1, endIdx);
            }
        }
    }

    private static int partition(int[] arr, int startIdx, int endIdx) {

        /* Choose the last value as pivot
        (different implementations are possible) */
        int pivotValue = arr[endIdx];

        /* Initialize the index where we will place
        an element if its value is smaller than the pivot */
        int smallerElementIdx = startIdx - 1;

        for (int i = startIdx; i < endIdx; i++) {

            int current = arr[i];

            /* If the current value is smaller than the pivot, we increase
            the reference index and move the current value there, effectively
            placing it in the left partition */
            if (current < pivotValue) {
                IntArrayUtils.swap(arr, ++smallerElementIdx, i);
            }
        }

        /* We move the pivot value (which was at endIdx) to the right
        of the left partition, effectively placing it between the two partitions */
        IntArrayUtils.swap(arr, smallerElementIdx + 1, endIdx);

        // We return the index of the pivot value
        return smallerElementIdx + 1;
    }
}
