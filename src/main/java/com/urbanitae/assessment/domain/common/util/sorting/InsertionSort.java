package com.urbanitae.assessment.domain.common.util.sorting;

import org.springframework.util.Assert;

/**
 * <p>Implements Insertion Sort algorithm
 * to sort an array of integers.</p>
 *
 * <p>This algorithm offers a linear O(n) performance for the
 * smallest arrays, but degrades significantly for the average cases,
 * so you should consider using {@link HybridQuickSort}
 * or {@link HybridMergeSort} for medium size or large datasets</p>
 *
 * @author David G. Ortiz
 */
public final class InsertionSort {

    private InsertionSort() {
    }

    /**
     * Sorts a given int array by using
     * the InsertionSort algorithm
     *
     * @param arr the int array to sort
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr) {

        Assert.notNull(arr, "An array is required");

        sort(arr, 0, arr.length - 1);
    }

    /**
     * Sorts a given int array from a
     * startIdx to an endIdx by using the
     * InsertionSort algorithm
     *
     * @param arr the int array to sort
     * @param startIdx the starting index
     * @param endIdx the ending index
     * @throws IllegalArgumentException if arr is null
     */
    public static void sort(int[] arr, int startIdx, int endIdx) {

        Assert.notNull(arr, "An array is required");

        /*
        Traverse the array to compare each value
        with its predecessors, moving to the right those
        that are greater until we find one that is smaller
         */
        for (int i = startIdx + 1; i <= endIdx; ++i) {

            int current = arr[i];
            int prevIdx = i - 1;

            /*
            While there's still a previous index in the range
            to be sorted, if its value is greater than the
            current value, move that value one spot to the
            right to make room for the current value
            */
            while (prevIdx >= startIdx && arr[prevIdx] > current) {
                arr[prevIdx + 1] = arr[prevIdx];
                prevIdx--;
            }

            /*
            Place the current value after the prevIndex,
            which is immediately before all moved values
            (or in its initial place if none moved)
             */
            arr[prevIdx + 1] = current;
        }
    }
}
