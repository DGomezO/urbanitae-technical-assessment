package com.urbanitae.assessment.application.words;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class with methods to find
 * matching words
 *
 * @author David G. Ortiz
 */
@Service
public class MatchingWordsFinder {

    /**
     * <p>Finds the matching words between
     * two given sentences</p>
     *
     * @param first the first sentence to compare
     * @param second the second sentence to compare
     * @throws IllegalArgumentException if any of the given sentences is null
     * @return the matching words between the given sentences
     */
    public String[] find(String first, String second) {

        Assert.notNull(first, "The sentences to check must not be null");
        Assert.notNull(second, "The sentences to check must not be null");

        // Initialize a map to store the words found in the first sentence
        Map<String, Integer> found = new HashMap<>();
        // Initialize a list to store the matching words
        List<String> matchingWords = new ArrayList<>();

        // Store the count for every word found in the first sentence
        for (String word : first.split(" ")) {
            found.put(word, found.getOrDefault(word, 0) + 1);
        }

        /* For every word in the second sentence, if the count for
        that word in the first sentence is more than 0, add the
        word to the matching words array and decrement its count
        in the map of found words */
        for (String word: second.split(" ")) {
            if(found.getOrDefault(word, 0) > 0) {
                matchingWords.add(word);
                found.put(word, found.getOrDefault(word, 0) - 1);
            }
        }

        // Return the matching words as an array
        return matchingWords.toArray(String[]::new);
    }
}
