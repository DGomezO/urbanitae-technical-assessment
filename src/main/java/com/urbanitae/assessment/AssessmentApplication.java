package com.urbanitae.assessment;

import com.urbanitae.assessment.application.words.MatchingWordsFinder;
import com.urbanitae.assessment.domain.common.util.IntArrayUtils;
import com.urbanitae.assessment.domain.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class AssessmentApplication implements CommandLineRunner {

	Logger LOG = LoggerFactory.getLogger(AssessmentApplication.class);

	private final MatchingWordsFinder matchingWordsFinder;

	public AssessmentApplication(MatchingWordsFinder matchingWordsFinder) {
		this.matchingWordsFinder = matchingWordsFinder;
	}

	public static void main(String[] args) {
		SpringApplication.run(AssessmentApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		LOG.info("Dada la siguiente cadena, dale la vuelta: Im just a good programmer with great habits");
		String reversed = StringUtils.reverseBy("Im just a good programmer with great habits", ' ');
		LOG.info("Resultado: {}", reversed);

		LOG.info("Ver si las siguiente cadena es palíndromo");
		LOG.info("Oso: {}", StringUtils.isPalindrome("Oso"));
		LOG.info("house: {}", StringUtils.isPalindrome("house"));

		LOG.info("Ordenar de menor a mayor una lista de números [7,9,78,5,4]");
		int[] arr = new int[]{7,9,78,5,4};
		IntArrayUtils.sort(arr);
		LOG.info("Resultado: {}", arr);

		LOG.info("Mostrar por consola las palabras repetidas de las siguientes frases:");
		String first = "El cielo está enladrillado quien lo desenladrillará";
		String second = "El cielo está encapotado quien lo desencapotará";
		LOG.info(first);
		LOG.info(second);
		String[] matchingWords = matchingWordsFinder.find(first, second);
		LOG.info("Resultado: {}", Arrays.toString(matchingWords));
	}
}
