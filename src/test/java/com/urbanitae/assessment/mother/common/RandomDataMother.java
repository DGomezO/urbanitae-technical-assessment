package com.urbanitae.assessment.mother.common;

import com.github.javafaker.Faker;

/**
 * Provides a random data generator for
 * testing based on {@link Faker} library
 *
 * @author David G. Ortiz
 */
public final class RandomDataMother {

    private static final Faker FAKER = new Faker();

    /**
     * Provides a random data generator for
     * testing based on {@link Faker} library
     *
     * @return a random data generator based on {@link Faker}
     */
    public static Faker random() {
        return FAKER;
    }
}
