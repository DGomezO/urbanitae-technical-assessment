package com.urbanitae.assessment.mother.common;

/**
 * A tuple of values to serve as arguments
 * for a test that requires a source value
 * and an expected value
 *
 * @author David G. Ortiz
 */
public final class TestArguments<S, E> {

    private final S source;

    private final E expected;

    private TestArguments(S source, E expected) {
        this.source = source;
        this.expected = expected;
    }

    /**
     * A tuple of values to serve as arguments
     * for a test that requires a source value
     * and an expected value
     *
     * @param source the source value
     * @param expected the expected value
     * @return A tuple of source and expected values
     */
    public static <S, E> TestArguments<S, E> of(S source, E expected) {
        return new TestArguments<>(source, expected);
    }

    public S getSource() {
        return source;
    }

    public E getExpected() {
        return expected;
    }
}
