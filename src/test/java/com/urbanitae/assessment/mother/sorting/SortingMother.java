package com.urbanitae.assessment.mother.sorting;

import com.urbanitae.assessment.domain.common.util.IntArrayUtils;
import com.urbanitae.assessment.mother.common.RandomDataMother;
import com.urbanitae.assessment.mother.common.TestArguments;

import java.util.stream.IntStream;

/**
 * Generates random data or arguments for tests
 * that involve sorting of arrays of integers
 *
 * @author David G. Ortiz
 */
public final class SortingMother {

    /**
     * Generates random arguments for tests that
     * receive an unsorted array of integers
     * and expect a sorted array
     *
     * @param sequenceSize the size of the sequences to generate
     * @return a tuple of test arguments consistent of a sorted and an unsorted array
     */
    public static TestArguments<int[], int[]> randomSequenceArguments(int sequenceSize) {
        var expected = SortingMother.randomSequence(sequenceSize);
        var shuffled = IntArrayUtils.copyOf(expected);
        IntArrayUtils.shuffle(shuffled);
        return TestArguments.of(shuffled, expected);
    }

    /**
     * Generates a random sorted sequence of integers
     *
     * @param size the size of the sequence to generate
     * @return a random sorted sequence of integers
     */
    public static int[] randomSequence(int size) {
        var start = RandomDataMother.random().number().numberBetween(0, Integer.MAX_VALUE - size);
        return IntStream.range(start, start + size).toArray();
    }
}
