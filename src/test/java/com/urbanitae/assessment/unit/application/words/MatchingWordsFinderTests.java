package com.urbanitae.assessment.unit.application.words;

import com.urbanitae.assessment.application.words.MatchingWordsFinder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class MatchingWordsFinderTests {

    private final MatchingWordsFinder finder;

    public MatchingWordsFinderTests() {
        this.finder = new MatchingWordsFinder();
    }

    @ParameterizedTest
    @CsvSource({
            "El cielo está enladrillado quien lo desenladrillará, El cielo está encapotado quien lo desencapotará, El cielo está quien lo",
            "count count count count, count count, count count",
            "count count, count count count count, count count",
            "CaseMatters, caseMatters, ",
            "CaseMatters caseMatters casematters, CaseMatters caseMatters casematters, CaseMatters caseMatters casematters"
    })
    void should_find_the_matching_words_between_two_sentences(String first, String second, String expectedWords) {

        var expected = expectedWords != null ? expectedWords.split(" ") : new String[0];
        var actual = finder.find(first, second);

        assertArrayEquals(expected, actual);
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_any_of_the_given_sentences_is_null(String str) {

        var e = assertThrows(IllegalArgumentException.class, () -> finder.find(str, str));

        assertEquals("The sentences to check must not be null", e.getMessage());
    }
}
