package com.urbanitae.assessment.unit.domain.common.util.sorting;

import com.urbanitae.assessment.domain.common.util.sorting.HybridMergeSort;
import com.urbanitae.assessment.mother.common.TestArguments;
import com.urbanitae.assessment.mother.sorting.SortingMother;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HybridMergeSortTests {

    @ParameterizedTest
    @MethodSource("buildTestArguments")
    void should_sort_an_array_of_integers(TestArguments<int[], int[]> args) {

        HybridMergeSort.sort(args.getSource());

        assertArrayEquals(args.getExpected(), args.getSource());
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_sorting_an_int_array(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> HybridMergeSort.sort(arr));

        assertEquals("An array is required", e.getMessage());
    }

    private static Stream<TestArguments<int[], int[]>> buildTestArguments() {
        return Stream.of(
                TestArguments.of(new int[]{7, 9, 78, 5, 4}, new int[]{4, 5, 7, 9, 78}),
                SortingMother.randomSequenceArguments(10),
                SortingMother.randomSequenceArguments(125),
                SortingMother.randomSequenceArguments(250),
                SortingMother.randomSequenceArguments(500),
                SortingMother.randomSequenceArguments(1000),
                SortingMother.randomSequenceArguments(5000)
        );
    }
}
