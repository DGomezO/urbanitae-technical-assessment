package com.urbanitae.assessment.unit.domain.common.util;

import com.urbanitae.assessment.domain.common.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTests {

    @ParameterizedTest
    @ValueSource(strings = {"oso", "radar", "luz azul", "Sé verlas al revés"})
    void should_return_true_if_a_string_is_a_palindrome(String str) {
        Assertions.assertTrue(StringUtils.isPalindrome(str));
    }

    @ParameterizedTest
    @ValueSource(strings = {"house", "homo", "lave vela"})
    void should_return_false_if_a_string_is_not_a_palindrome(String str) {
        assertFalse(StringUtils.isPalindrome(str));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void should_return_false_checking_palindrome_if_null_or_empty_string(String str) {
        assertFalse(StringUtils.isPalindrome(str));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "Im just a good programmer with great habits:' ':habits great with programmer good a just Im",
            "I like parametrized tests to try different inputs:' ':inputs different try to tests parametrized like I",
            "If I use commas, they don't move:' ':move don't they commas, use I If",
            "It_works_with_a_different_delimiter:'_':delimiter_different_a_with_works_It"
    }, delimiter = ':')
    void should_reverse_by_whitespace_delimiter(String str, char delimiter, String expected) {

        var actual = StringUtils.reverseBy(str, delimiter);

        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @NullSource
    void should_return_null_if_reversing_null_by_delimiter(String str) {

        var actual = StringUtils.reverseBy(str, ' ');

        assertNull(actual);
    }

    @ParameterizedTest
    @EmptySource
    void should_return_an_empty_string_if_reversing_an_empty_string_by_delimiter(String str) {

        var actual = StringUtils.reverseBy(str, ' ');

        assertEquals("", actual);
    }

    @ParameterizedTest
    @CsvSource({"programming,gnimmargorp", "This is code,edoc si sihT",})
    void should_reverse_a_string(String str, String expected) {

        var actual = StringUtils.reverse(str);

        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @NullSource
    void should_return_null_if_reversing_null(String str) {

        var actual = StringUtils.reverse(str);

        assertNull(actual);
    }

    @ParameterizedTest
    @EmptySource
    void should_return_an_empty_string_if_reversing_an_empty_string(String str) {

        var actual = StringUtils.reverse(str);

        assertEquals("", actual);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void should_determine_that_a_string_is_blank(String str) {
        assertTrue(StringUtils.isBlank(str));
    }

    @ParameterizedTest
    @ValueSource(strings = {"word", "two words", "blank", "null"})
    void should_determine_that_a_string_is_not_blank(String str) {
        assertFalse(StringUtils.isBlank(str));
    }

    @ParameterizedTest
    @CsvSource({"equal,Equal", "Utility methods are good,utility Methods are Good",})
    void should_return_true_if_two_strings_are_equals_ignoring_case(String str1, String str2) {
        assertTrue(StringUtils.equalsIgnoreCase(str1, str2));
    }

    @ParameterizedTest
    @CsvSource({"equal,distinct", "This is a sentence,This is another", "not null,"})
    void should_return_false_if_two_strings_are_not_equals_ignoring_case(String str1, String str2) {
        assertFalse(StringUtils.equalsIgnoreCase(str1, str2));
    }

    @ParameterizedTest
    @NullSource
    void should_return_true_checking_equality_for_two_nulls(String str) {
        assertTrue(StringUtils.equalsIgnoreCase(str, str));
    }
}
