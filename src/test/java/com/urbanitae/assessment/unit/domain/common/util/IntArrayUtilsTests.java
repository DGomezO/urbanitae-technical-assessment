package com.urbanitae.assessment.unit.domain.common.util;

import com.urbanitae.assessment.domain.common.util.IntArrayUtils;
import com.urbanitae.assessment.mother.common.TestArguments;
import com.urbanitae.assessment.mother.sorting.SortingMother;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class IntArrayUtilsTests {

    @ParameterizedTest
    @MethodSource("buildTestArguments")
    void should_sort_an_array_of_integers(TestArguments<int[], int[]> args) {

        IntArrayUtils.sort(args.getSource());

        assertArrayEquals(args.getExpected(), args.getSource());
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_sorting_an_int_array(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.sort(arr));

        assertEquals("An array is required", e.getMessage());
    }

    @ParameterizedTest
    @MethodSource("buildSequences")
    void should_shuffle_an_int_array(int[] arr) {

        var copy = IntArrayUtils.copyOf(arr);

        IntArrayUtils.shuffle(copy);

        assertNotEquals(arr, copy);
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_shuffling_an_int_array(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.shuffle(arr));

        assertEquals("An array is required", e.getMessage());
    }

    @Test
    void should_swap_two_elements_of_an_int_array() {

        var argsStream = Stream.of(
                Map.of("source", new int[]{3, 7, 9, 2, 8, 5}, "expected", new int[]{3, 7, 5, 2, 8, 9}, "first", 2, "second", 5),
                Map.of("source", new int[]{82, 41, 27, 78, 53, 36}, "expected", new int[]{36, 41, 27, 78, 53, 82}, "first", 0, "second", 5),
                Map.of("source", new int[]{1, 1, 1}, "expected", new int[]{1, 1, 1}, "first", 1, "second", 2),
                Map.of("source", new int[]{3, 4, 5, 5, 4, 3}, "expected", new int[]{3, 4, 5, 5, 4, 3}, "first", 1, "second", 4)
        );

        argsStream.forEach(args -> {
            var source = (int[]) args.get("source");
            var expected = (int[]) args.get("expected");
            var firstIdx = (int) args.get("first");
            var secondIdx = (int) args.get("second");

            IntArrayUtils.swap(source, firstIdx, secondIdx);

            assertArrayEquals(expected, source);
        });
    }

    @Test
    void should_throw_if_index_to_swap_are_out_of_bounds() {

        var arr = new int[]{1, 2, 3};

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> IntArrayUtils.swap(arr, 0, 4));
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_swapping_elements(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.swap(arr, 1, 4));

        assertEquals("An array is required", e.getMessage());
    }

    @ParameterizedTest
    @MethodSource("buildSequences")
    void should_copy_an_int_array(int[] original) {

        var actual = IntArrayUtils.copyOf(original);

        assertNotEquals(original, actual);
        assertArrayEquals(original, actual);
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_copying_an_int_arr(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.copyOf(arr));

        assertEquals("An array is required", e.getMessage());
    }

    @Test
    void should_copy_an_int_array_from_start_idx_to_end_idx() {

        var argsStr = Stream.of(
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{1, 2}, "start", 0, "end", 2),
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{1, 2, 3, 4}, "start", 0, "end", 4),
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{3, 4, 5}, "start", 2, "end", 5),
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{1, 2, 3, 4, 5}, "start", 0, "end", 5),
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{1}, "start", 0, "end", 1),
                Map.of("original", new int[]{1, 2, 3, 4, 5}, "expected", new int[]{}, "start", 0, "end", 0)
        );

        argsStr.forEach(args -> {

            var original = (int[]) args.get("original");
            var expected = (int[]) args.get("expected");
            var startIdx = (int) args.get("start");
            var endIdx = (int) args.get("end");

            var actual = IntArrayUtils.copyOf(original, startIdx, endIdx);

            assertNotEquals(original, actual);
            assertArrayEquals(expected, actual);
        });
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_copying_an_int_arr_from_start_idx_to_end_idx(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.copyOf(arr, 1, 3));

        assertEquals("An array is required", e.getMessage());
    }

    @Test
    void should_throw_if_index_to_copy_and_array_are_out_of_bounds() {

        var arr = new int[]{1, 2, 3};

        assertThrows(ArrayIndexOutOfBoundsException.class, () -> IntArrayUtils.copyOf(arr, 0, 4));
    }

    @Test
    void should_throw_if_start_idx_is_greater_than_end_idx_when_copying_an_int_array() {

        var arr = new int[]{1, 2, 3};

        var e = assertThrows(IllegalArgumentException.class, () -> IntArrayUtils.copyOf(arr, 2, 1));

        assertEquals("The startIdx cannot be greater than the endIdx", e.getMessage());
    }

    private static Stream<TestArguments<int[], int[]>> buildTestArguments() {
        return Stream.of(
                TestArguments.of(new int[]{7, 9, 78, 5, 4}, new int[]{4, 5, 7, 9, 78}),
                SortingMother.randomSequenceArguments(10),
                SortingMother.randomSequenceArguments(25),
                SortingMother.randomSequenceArguments(50),
                SortingMother.randomSequenceArguments(125),
                SortingMother.randomSequenceArguments(250),
                SortingMother.randomSequenceArguments(500),
                SortingMother.randomSequenceArguments(1000),
                SortingMother.randomSequenceArguments(5000)
        );
    }

    private static Stream<int[]> buildSequences() {
        return Stream.of(
                SortingMother.randomSequence(10),
                SortingMother.randomSequence(25),
                SortingMother.randomSequence(50),
                SortingMother.randomSequence(125),
                SortingMother.randomSequence(250),
                SortingMother.randomSequence(500)
        );
    }
}
