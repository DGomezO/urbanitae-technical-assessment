package com.urbanitae.assessment.unit.domain.common.util.sorting;

import com.urbanitae.assessment.domain.common.util.sorting.HybridQuickSort;
import com.urbanitae.assessment.mother.common.TestArguments;
import com.urbanitae.assessment.mother.sorting.SortingMother;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HybridQuickSortTests {

    @ParameterizedTest
    @MethodSource("buildTestArguments")
    void should_sort_an_array_of_integers(TestArguments<int[], int[]> args) {

        HybridQuickSort.sort(args.getSource());

        assertArrayEquals(args.getExpected(), args.getSource());
    }

    @ParameterizedTest
    @NullSource
    void should_throw_if_source_is_null_when_sorting_an_int_array(int[] arr) {

        var e = assertThrows(IllegalArgumentException.class, () -> HybridQuickSort.sort(arr));

        assertEquals("An array is required", e.getMessage());
    }

    @Test
    void should_sort_an_array_of_integers_from_start_idx_to_end_idx() {

        var argsStr = Stream.of(
                Map.of("original", new int[]{7, 9, 78, 5, 4}, "expected", new int[]{4, 5, 7, 9, 78}, "start", 0, "end", 4),
                Map.of("original", new int[]{7, 9, 78, 5, 4}, "expected", new int[]{7, 9, 78, 5, 4}, "start", 0, "end", 2),
                Map.of("original", new int[]{7, 9, 78, 5, 4}, "expected", new int[]{7, 9, 4, 5, 78}, "start", 2, "end", 4),
                Map.of("original", new int[]{7, 9, 78, 5, 4}, "expected", new int[]{7, 9, 78, 4, 5}, "start", 3, "end", 4)
        );

        argsStr.forEach(args -> {
            var original = (int[]) args.get("original");
            var expected = (int[]) args.get("expected");
            var startIdx = (int) args.get("start");
            var endIdx = (int) args.get("end");

            HybridQuickSort.sort(original, startIdx, endIdx);

            assertArrayEquals(expected, original);
        });
    }

    private static Stream<TestArguments<int[], int[]>> buildTestArguments() {
        return Stream.of(
                TestArguments.of(new int[]{7, 9, 78, 5, 4}, new int[]{4, 5, 7, 9, 78}),
                SortingMother.randomSequenceArguments(10),
                SortingMother.randomSequenceArguments(25),
                SortingMother.randomSequenceArguments(50),
                SortingMother.randomSequenceArguments(125),
                SortingMother.randomSequenceArguments(250),
                SortingMother.randomSequenceArguments(500)
        );
    }
}
