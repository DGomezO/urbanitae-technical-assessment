# URBANITAE - PRUEBA TÉCNICA
### Prueba técnica para el proceso de selección de Urbanitae

#### INICIO RÁPIDO

Para la resolución de los ejercicios propuestos se ha desarrollado una aplicación Spring Boot que implementa en su main class la interfaz
CommandLineRunner para imprimir al arrancar el enunciado y el resultado de cada uno de los ejercicios. Se ha incluido un target en el Makefile del proyecto:

```make run```

Todo el código desarrollado para la resolución de los distintos ejercicios se ha respaldado con tests unitarios. Para ejecutar la suite 
de tests también se ha incluido un target en el Makefile del proyecto:

```make test```

#### DETALLE DE LA SOLUCIÓN

En general, para la resolución de los ejercicios se ha optado por utilizar el mínimo código de terceros e implementar toda la funcionalidad
desde cero, incluso si la solución más sencilla o directa pudiera pasar por recurrir a métodos de librerías. El objetivo es mostrar el conocimiento de
los algoritmos necesarios para resolver de forma eficiente el ejercicio. Con ese mismo fin, se han incluido comentarios a lo largo del código
que explican paso a paso la solución propuesta.

- Para el primer ejercicio (dar la vuelta a una cadena de texto), se ha optado por crear un método estático en una clase de utilidad (StringUtils).
Dicho método (reverseBy) permite no solo revertir la frase con el espacio en blanco como delimitador, sino también hacerlo con otros delimitadores,
ofreciendo una mayor versatilidad sin disminuir el rendimiento.
- Para el segundo ejercicio (comprobar si una cadena es un palíndromo), se ha optado por crear un método estático en una clase de utilidad (StringUtils).
Dicho método (isPalindrome) permite no solo chequear palabras, sino también frases enteras, como se puede comprobar en los tests unitarios.
- Para el tercer ejercicio (ordenar un array de números), se ha optado por crear un método estático en una clase de utilidad (IntArrayUtils).
Dicho método (sort) recurre a tres algoritmos de ordenación distintos en función del tamaño del array o la partición: InsertionSort, QuickSort y MergeSort.
Los tres algoritmos se han implementado manualmente y se combinan para formar un algoritmo híbrido que emplea la solución más eficiente para cada tamaño de
array o partición.
- Para el cuarto ejercicio (encontrar palabras repetidas entre dos frases), se ha optado por crear un application service (MatchingWordsFinder) que se inyecta 
en la main class de la aplicación para calcular el resultado y mostrarlo por consola. Como se puede comprobar en los tests, se ha decidido que la implementación sea case
sensitive y que pueda devolver varias veces una misma palabra como repetida si aparece en múltiples ocasiones en ambas frases.
